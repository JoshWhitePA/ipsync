﻿Imports System.Net.Mail
Imports System.Net
Imports System.Text.RegularExpressions
Imports System
Imports System.IO
Imports System.Text

Public Class frmEmail


    Dim info As New ArrayList()
    Dim frm As Form1
    Private Smtp_Server As New SmtpClient()
    Dim email As New MailMessage
    Dim smtp As New SmtpClient
    Dim mail As System.Net.Mail.MailAddress


    Private Sub email_send_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles email_send.Click
        Dim ipAd = IPGetter.GetExternalIP
        SendEmail.sendMail(txtEmail.Text, txtPassword.Text, CInt(txtPort.Text), txtSMTP.Text, "Update IP", ipAd)
        WriteToFile()
    End Sub

    Sub WriteToFile()
        Dim path As String = "C:\Users\Josh\AppData\Roaming\jSoft\info.txt"

        ' Create or overwrite the file. 
        Dim fs As FileStream = File.Create(path)

        ' Add text to the file. 
        Dim info As Byte() = New UTF8Encoding(True).GetBytes(txtEmail.Text + "|" + txtPassword.Text + "|" + txtPort.Text + "|" + txtSMTP.Text + "|" + IPGetter.currentIP)
        fs.Write(info, 0, info.Length)
        fs.Close()
    End Sub


    Private Sub frmEmail_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not My.Computer.FileSystem.DirectoryExists("C:\Users\Josh\AppData\Roaming\jSoft") Then
            My.Computer.FileSystem.CreateDirectory("C:\Users\Josh\AppData\Roaming\jSoft")
        End If

        delimitString()
        Dim count = 0
        For Each textFromUser In info
            Select Case count
                Case 0
                    txtEmail.Text = textFromUser
                Case 1
                    txtPassword.Text = textFromUser
                Case 2
                    txtPort.Text = textFromUser
                Case 3
                    txtSMTP.Text = textFromUser
                Case 4
                    txtIP.Text = textFromUser
            End Select
            count = count + 1
        Next


    End Sub

    Sub DelimitString()
        Using MyReader As New Microsoft.VisualBasic.
                      FileIO.TextFieldParser(
                        "C:\Users\Josh\AppData\Roaming\jSoft\info.txt")
            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters("|")
            Dim currentRow As String()
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    Dim currentField As String
                    For Each currentField In currentRow
                        info.Add(currentField)
                    Next
                Catch ex As Microsoft.VisualBasic.
                            FileIO.MalformedLineException
                    MsgBox("Line " & ex.Message &
                    "is not valid and will be skipped.")
                End Try
            End While
        End Using
    End Sub

End Class

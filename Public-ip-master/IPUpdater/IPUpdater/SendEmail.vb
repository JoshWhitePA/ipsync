﻿Imports System.Net.Mail

Module SendEmail
    Dim info As New ArrayList()
    Dim frm As Form1
    Private Smtp_Server As New SmtpClient()
    Dim email As New MailMessage
    Dim smtp As New SmtpClient
    Dim mail As System.Net.Mail.MailAddress

    Public Sub sendMail(emailAd As String, password As String, porttxt As Integer, SMTPtxt As String, subject As String, ipAd As String)
        Try
            email.From = New MailAddress(emailAd)
            email.Subject = subject
            email.Body = ipAd
            email.To.Add(emailAd)
            smtp.EnableSsl = True
            smtp.Port = CInt(porttxt)
            smtp.Host = SMTPtxt
            smtp.Credentials = New Net.NetworkCredential(emailAd, password)
            smtp.Send(email)
            MsgBox("The e-mail has been sent!")
        Catch e As Exception
            MessageBox.Show(e.ToString)

        End Try
    End Sub
End Module

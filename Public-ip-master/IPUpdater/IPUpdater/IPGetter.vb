﻿Imports System.Text.RegularExpressions
Imports System.Net
Imports System

Module IPGetter

    Public currentIP As String

    Public Function GetExternalIp() As String
        Try
            Dim ExternalIP As String
            ExternalIP = (New WebClient()).DownloadString("http://checkip.dyndns.org/")
            ExternalIP = (New Regex("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")) _
                         .Matches(ExternalIP)(0).ToString()
            currentIP = ExternalIP
            Return ExternalIP

        Catch
            Return Nothing
        End Try
    End Function
End Module
